package com.tkcodes.gadgeothek;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertEquals;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class LoginActivityInstrumentationTest {

    @Rule
    public ActivityTestRule loginActivityRule = new ActivityTestRule(LoginActivity.class);

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.tkcodes.gadgeothek", appContext.getPackageName());
    }

    @Test
    public void testLogin(){
        String email = "test@example.com";
        String password = "test12";

        // validate input
        onView(withId(R.id.email))
                .perform(clearText(), typeText(email), closeSoftKeyboard());
        onView(withId(R.id.password))
                .perform(clearText(), typeText(password), closeSoftKeyboard());
        onView(withId(R.id.email_sign_in_button))
                .perform(click());

        // check if teaser image is displayed after successful login
        onView(withId(R.id.ivTeaser)).check(matches(isDisplayed()));
    }


}
