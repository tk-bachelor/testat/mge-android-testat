package com.tkcodes.gadgeothek.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tkcodes.gadgeothek.R;
import com.tkcodes.gadgeothek.domain.Reservation;
import com.tkcodes.gadgeothek.helper.ViewHelper;

import java.util.ArrayList;
import java.util.List;

public class ReservationAdapter extends RecyclerView.Adapter<ReservationAdapter.ViewHolder> {
    private List<Reservation> reservations;

    public ReservationAdapter() {
        this.reservations = new ArrayList<>();
    }

    @Override
    public ReservationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        return new ViewHolder(layoutInflater.inflate(R.layout.content_reservation_row, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Reservation reservation = this.reservations.get(position);
        holder.tvName.setText(reservation.getGadget().getName());
        holder.tvPrimary.setText(String.format("%s - %s", ViewHelper.formatPrice(reservation.getGadget().getPrice()), reservation.getGadget().getCondition()));
        holder.tvSecondary.setText(String.format("Inventory Number: %s by %s", reservation.getGadget().getInventoryNumber(), reservation.getGadget().getManufacturer()));
        holder.tvTimestamp.setText(ViewHelper.formatShortDate(reservation.getReservationDate()));

        holder.ivIconBg.setImageResource(R.drawable.bg_circle);
        holder.ivIconBg.setColorFilter(ViewHelper.getRandomColor());
        holder.ivIconBg.setVisibility(View.VISIBLE);
        holder.tvIconText.setText(String.format("%d", reservation.getWatingPosition()));
        holder.tvIconDesc.setText("Position");

        holder.ivStatus.setBackgroundResource(reservation.isReady() ? R.drawable.ic_ready : R.drawable.ic_waiting);
    }

    @Override
    public int getItemCount() {
        return this.reservations.size();
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName, tvPrimary, tvSecondary,
                tvTimestamp, tvIconText, tvIconDesc;
        public ImageView ivIconBg, ivStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            this.tvName = itemView.findViewById(R.id.tvGadget);
            this.tvPrimary = itemView.findViewById(R.id.tvPrimary);
            this.tvSecondary = itemView.findViewById(R.id.tvSecondary);
            this.tvTimestamp = itemView.findViewById(R.id.tvTimestamp);
            this.tvIconText = itemView.findViewById(R.id.tvIconText);
            this.ivIconBg = itemView.findViewById(R.id.ivIconBg);
            this.tvIconDesc = itemView.findViewById(R.id.tvIconDesc);
            this.ivStatus = itemView.findViewById(R.id.ivStatus);
        }
    }

    public Reservation getReservation(int position){
        return this.reservations.get(position);
    }

    public void removeReservation(int position){
        this.reservations.remove(position);
        this.notifyItemRemoved(position);
    }
}
