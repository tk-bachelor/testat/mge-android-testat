package com.tkcodes.gadgeothek.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tkcodes.gadgeothek.R;
import com.tkcodes.gadgeothek.domain.Loan;
import com.tkcodes.gadgeothek.helper.ViewHelper;
import com.tkcodes.gadgeothek.helper.ViewTimeUnit;

import java.util.ArrayList;
import java.util.List;

public class LoansAdapter extends RecyclerView.Adapter<LoansAdapter.ViewHolder> {
    private List<Loan> loans;

    public LoansAdapter() {
        this.loans = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.content_loan_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Loan loan = this.loans.get(position);
        holder.tvName.setText(loan.getGadget().getName());
        holder.tvPrimary.setText(String.format("%s", ViewHelper.formatPrice(loan.getGadget().getPrice())));
        holder.tvSecondary.setText(String.format("Inventory Number: %s \nby %s", loan.getGadget().getInventoryNumber(), loan.getGadget().getManufacturer()));
        holder.tvTimestamp.setText(ViewHelper.formatShortDate(loan.getPickupDate()));

        // remaining time to due date
        long remainingDays = ViewHelper.getRemainingDays(loan.overDueDate());
        ViewTimeUnit unit = ViewHelper.getAppropriateTimeUnit(remainingDays);

        holder.ivIconBg.setImageResource(R.drawable.bg_circle);
        holder.ivIconBg.setColorFilter(ViewHelper.getRandomColor());
        holder.ivIconBg.setVisibility(View.VISIBLE);
        holder.tvIconText.setText(String.format("%d", Math.abs(ViewHelper.getApproximate(remainingDays, unit))));
        holder.tvIconDesc.setText(String.format("%s \n%s", unit.toString().toLowerCase(), remainingDays > 0 ? "left" : "overdue"));

        holder.tvCondition.setText(loan.getGadget().getCondition().toString());
    }

    @Override
    public int getItemCount() {
        return this.loans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName, tvPrimary, tvSecondary,
                tvTimestamp, tvIconText, tvCondition, tvIconDesc;
        public ImageView ivIconBg;

        public ViewHolder(View itemView) {
            super(itemView);
            this.tvName = itemView.findViewById(R.id.tvGadget);
            this.tvPrimary = itemView.findViewById(R.id.tvPrimary);
            this.tvSecondary = itemView.findViewById(R.id.tvSecondary);
            this.tvTimestamp = itemView.findViewById(R.id.tvTimestamp);
            this.tvIconText = itemView.findViewById(R.id.tvIconText);
            this.ivIconBg = itemView.findViewById(R.id.ivIconBg);
            this.tvCondition = itemView.findViewById(R.id.tvCondition);
            this.tvIconDesc = itemView.findViewById(R.id.tvIconDesc);
        }
    }

    public void setLoans(List<Loan> loans) {
        this.loans = loans;
    }
}
