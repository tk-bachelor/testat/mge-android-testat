package com.tkcodes.gadgeothek.fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.tkcodes.gadgeothek.R;
import com.tkcodes.gadgeothek.domain.Gadget;
import com.tkcodes.gadgeothek.helper.ViewHelper;
import com.tkcodes.gadgeothek.service.Callback;
import com.tkcodes.gadgeothek.service.LibraryService;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link NewReservationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NewReservationFragment extends DialogFragment {
    public static final String ARG_GADGETS = "ITEM_GADGETS";
    private List<Gadget> gadgets;

    private ProgressBar progressBar;
    private ListView lvGadgets;
    private int selectedPos = -1;

    private NewReservationDialogListener callback;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.gadgets = new ArrayList<>();
    }

    public static NewReservationFragment newInstance() {
        NewReservationFragment fragment = new NewReservationFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (!(context instanceof NewReservationDialogListener)) {
            throw new IllegalStateException("Activity must implement NewReservationDialogListener");
        }
        callback = (NewReservationDialogListener) context;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Reserve your gadget");

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View rootView = inflater.inflate(R.layout.dialog_new_reservation, null);
        progressBar = rootView.findViewById(R.id.progressBar);
        lvGadgets = rootView.findViewById(R.id.lvGadgets);
        lvGadgets.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedPos = i;
            }
        });

        loadGadgetsAsync(rootView);

        builder.setView(rootView);
        builder.setNegativeButton(android.R.string.cancel, null);
        builder.setPositiveButton("Reserve", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                reserve();
            }
        });

        return builder.create();
    }

    private void loadGadgetsAsync(View rootView) {
        LibraryService.getGadgets(new Callback<List<Gadget>>() {
            @Override
            public void onCompletion(List<Gadget> input) {
                if (input.size() > 0) {
                    gadgets = input;
                    String[] gadgetDescriptions = ViewHelper.getGadgetsForDialog(input);
                    ArrayAdapter<String> itemsAdapter = new ArrayAdapter<String>(
                            getActivity(),
                            android.R.layout.simple_list_item_single_choice, gadgetDescriptions);
                    lvGadgets.setAdapter(itemsAdapter);

                    lvGadgets.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                } else {
                    Toast.makeText(getActivity(), "No gadgets to reserve", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onError(String message) {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void reserve() {
        if (selectedPos >= 0) {
            final Gadget gadget = gadgets.get(selectedPos);
            LibraryService.reserveGadget(gadget, new Callback<Boolean>() {
                @Override
                public void onCompletion(Boolean input) {
                    callback.onReservationCompletion(gadget, input);
                }

                @Override
                public void onError(String message) {
                    callback.onReservationError(message);
                }
            });
        } else {
            Toast.makeText(getActivity(), "No gadgets selected", Toast.LENGTH_LONG).show();
        }
    }

    public interface NewReservationDialogListener {
        public void onReservationCompletion(Gadget gadget, boolean success);

        public void onReservationError(String message);
    }
}
