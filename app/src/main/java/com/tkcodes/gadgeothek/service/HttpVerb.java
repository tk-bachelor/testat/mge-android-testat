package com.tkcodes.gadgeothek.service;

enum HttpVerb {
    POST,
    GET,
    DELETE,
}
