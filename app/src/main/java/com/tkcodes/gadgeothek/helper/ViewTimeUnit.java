package com.tkcodes.gadgeothek.helper;

public enum ViewTimeUnit {
    DAYS, MONTHS, YEARS
}
