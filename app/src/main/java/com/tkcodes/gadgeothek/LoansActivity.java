package com.tkcodes.gadgeothek;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.tkcodes.gadgeothek.adapter.LoansAdapter;
import com.tkcodes.gadgeothek.domain.Loan;
import com.tkcodes.gadgeothek.service.Callback;
import com.tkcodes.gadgeothek.service.LibraryService;

import java.util.List;

public class LoansActivity extends AppCompatActivity {

    private RecyclerView rvLoans;
    private LoansAdapter loansAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loans);

        rvLoans = (RecyclerView) findViewById(R.id.rvLoans);
        rvLoans.setHasFixedSize(true);
        rvLoans.setLayoutManager(new LinearLayoutManager(this));

        loansAdapter = new LoansAdapter();
        rvLoans.setAdapter(loansAdapter);
        rvLoans.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        loadLoans();
    }

    private void loadLoans() {
        LibraryService.getLoansForCustomer(new Callback<List<Loan>>() {
            @Override
            public void onCompletion(List<Loan> input) {
                loansAdapter.setLoans(input);
                loansAdapter.notifyDataSetChanged();
            }

            @Override
            public void onError(String message) {

            }
        });
    }
}
