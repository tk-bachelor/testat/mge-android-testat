package com.tkcodes.gadgeothek;

import android.app.DialogFragment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.tkcodes.gadgeothek.adapter.ReservationAdapter;
import com.tkcodes.gadgeothek.domain.Gadget;
import com.tkcodes.gadgeothek.domain.Reservation;
import com.tkcodes.gadgeothek.fragments.NewReservationFragment;
import com.tkcodes.gadgeothek.helper.DividerItemDecoration;
import com.tkcodes.gadgeothek.service.Callback;
import com.tkcodes.gadgeothek.service.LibraryService;

import java.util.List;

public class ReservationsActivity extends AppCompatActivity implements NewReservationFragment.NewReservationDialogListener {

    private ProgressBar reservationProgress;
    private RecyclerView rvReservations;
    private ReservationAdapter reservationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservations);

        reservationProgress = (ProgressBar) findViewById(R.id.reservation_progress);
        rvReservations = (RecyclerView) findViewById(R.id.rvReservations);
        rvReservations.setLayoutManager(new LinearLayoutManager(this));
        rvReservations.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback =
                new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                    @Override
                    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                        return false;
                    }

                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                        final int position = viewHolder.getAdapterPosition(); //get position which is swipe
                        removeReservation(position);
                    }
                };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(rvReservations);

        reservationAdapter = new ReservationAdapter();
        rvReservations.setAdapter(reservationAdapter);

        FloatingActionButton fabNewReservation = (FloatingActionButton) findViewById(R.id.fabNewReservation);
        fabNewReservation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openNewReservationDialog();
            }
        });
        loadReservations();
    }

    private void loadReservations() {
        LibraryService.getReservationsForCustomer(new Callback<List<Reservation>>() {
            @Override
            public void onCompletion(List<Reservation> input) {
                reservationAdapter.setReservations(input);
                reservationAdapter.notifyDataSetChanged();
                rvReservations.setVisibility(View.VISIBLE);
                reservationProgress.setVisibility(View.GONE);
            }

            @Override
            public void onError(String message) {

            }
        });
    }

    private void openNewReservationDialog() {
        DialogFragment dialog = NewReservationFragment.newInstance();
        dialog.show(getFragmentManager(), "NewReservationFragment");
    }

    @Override
    public void onReservationCompletion(Gadget gadget, boolean success) {
        if (success) {
            Toast.makeText(this, String.format("%s is reserved successfully", gadget.getName()), Toast.LENGTH_LONG).show();
            loadReservations();
        } else {
            Snackbar.make(rvReservations,
                    String.format("Reservation failed for %s", gadget.getName()),
                    Snackbar.LENGTH_INDEFINITE).show();
        }
    }

    @Override
    public void onReservationError(String message) {
        Snackbar.make(rvReservations, message, Snackbar.LENGTH_LONG).show();
    }

    private void removeReservation(final int position) {
        Reservation toBeDeleted = reservationAdapter.getReservation(position);
        LibraryService.deleteReservation(toBeDeleted, new Callback<Boolean>() {
            @Override
            public void onCompletion(Boolean input) {
                if (input) {
                    reservationAdapter.removeReservation(position);
                } else {
                    reservationAdapter.notifyItemChanged(position);
                }
            }

            @Override
            public void onError(String message) {
                Snackbar.make(rvReservations, message, Snackbar.LENGTH_LONG).show();
            }
        });
    }
}
