package com.tkcodes.gadgeothek;

import com.tkcodes.gadgeothek.util.ValidationUtil;

import org.junit.Test;

import static org.junit.Assert.*;

public class ValidationUtilTest {
    @Test
    public void testIsPasswordValid() throws Exception {
        assertTrue(ValidationUtil.isPasswordValid("Hello"));
        assertFalse(ValidationUtil.isPasswordValid("Hell"));
    }
}